package com.kouakam.springmvc.controller;

import com.kouakam.springmvc.dao.CustomerDao;
import com.kouakam.springmvc.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    private CustomerDao customerDao;

    @Autowired
    public CustomerController(CustomerDao dao){
        this.customerDao = dao;
    }

    /**
     * show the form to create a new customer
     * @param customer the placeholder from the customer to be created
     * @return the jsp file name of the form
     */
    @RequestMapping("/new")
    public String formCustomer(@ModelAttribute("customer") Customer customer){
        return "create-customer";
    }

    /**
     * create and persist a customer to the db
     * @param customer the customer to be persist
     * @return the home page or error
     */
    @RequestMapping("/create")
    public String createCustomer(@Valid @ModelAttribute("customer")Customer customer,
                                 BindingResult result){
        if (result.hasErrors()) {
            return "create-customer";
        }
        this.customerDao.addCustomer(customer);
        return "redirect:/customer/list";
    }

    @RequestMapping("/list")
    public String getListCustomers(Model model){
        List<Customer> customers = this.customerDao.getCustomerList();
        model.addAttribute("customers", customers);
        return "customer-list";
    }
}
