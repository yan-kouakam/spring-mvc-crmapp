package com.kouakam.springmvc.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customer")
public class Customer {


    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int customerId;

    //first name of the customer
    @Column(name = "first_name")
    @NotNull
    @Size(min = 1, message = "This field is required")
    private String firstName;

    // last name of the customer
    @Column(name = "last_name") //mapping to the database table
    @NotNull                    // not null validation for the field
    @Size(min = 1, message = "This field is required") // required field validation constraint
    private String lastName;

    //email of the user
    @Column(name = "email")
    @NotNull
    @Size(min = 1, message = "This field is required")
    private String email;

    public Customer() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public int getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
        return String.format("Name: %s \n " +
                "Last Name:  %s \n " +
                "Email %s",this.getFirstName(),this.getLastName(),this.getEmail() );
    }
}
