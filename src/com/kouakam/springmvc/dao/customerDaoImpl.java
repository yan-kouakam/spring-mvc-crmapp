package com.kouakam.springmvc.dao;

import com.kouakam.springmvc.entity.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Repository
public class customerDaoImpl implements CustomerDao{
    private final SessionFactory sessionFactory;

    @Autowired
    public customerDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * get the list of customer in the database
     * @return Customer list
     */
    @Transactional
    @Override
    public List<Customer> getCustomerList(){
        Session session =this.sessionFactory.getCurrentSession(); // create a new section

        return session.createQuery("from Customer",Customer.class)
                //get the list of customer
                .getResultList();
    }

    /**
     * add the customer @param to the database
     * @param customer the customer to be added to the db
     * @return true if the customer is successfully added false otherwise
     */
    @Override
    @Transactional
    public void addCustomer(Customer customer) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(customer);


    }
}
