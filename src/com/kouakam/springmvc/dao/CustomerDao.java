package com.kouakam.springmvc.dao;

import com.kouakam.springmvc.entity.Customer;

import java.util.List;

public interface CustomerDao {
     List<Customer> getCustomerList();
     void addCustomer(Customer customer);
}
