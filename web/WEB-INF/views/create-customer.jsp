<%--
  Created by IntelliJ IDEA.
  User: kouakam
  Date: 08.05.2018
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
    <link   type="text/css"
            rel="${pageContext.request.contextPath}/resources/css/bootstrap.css">
</head>
<body>
<div class="container">
    <form:form modelAttribute="customer"
               action="/customer/create" method="post">
        <div class="form-group">
            <label for="firstName" class="form-control"> First Name:</label>
            <form:input path="firstName" id="firstName" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <label for="last" class="form-control"> Last Name:</label>
            <form:input path="lastName" id="last" cssClass="form-control"/>
        </div>
        <div class="form-group">
            <label for="email" class="form-control"> Email:</label>
            <form:input path="email" id="email" cssClass="form-control"/>
            <form:errors cssStyle="font: red 12px " path="email"> </form:errors>
        </div>
        <div class="form-group">
            <form:button type="submit" cssClass = "btn btn-primary"> Submit</form:button>
        </div>

    </form:form>
</div>
</body>
</html>
