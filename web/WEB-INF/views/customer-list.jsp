<%--
  Created by IntelliJ IDEA.
  User: kouakam
  Date: 05.05.2018
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css"
          rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link type="text/css"
          rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>
<h2> HELLO !!!!</h2> <br>
<h3>contextPath: ${pageContext.request.contextPath}</h3>
<h3>app request: ${pageContext.request}</h3>
<h3>app pageContext: ${pageContext}</h3>
<a href="${pageContext.request.contextPath}/customer/new"> create new</a>
<div class="row">
    <div class="col-sm-push-12">
        <div class="header">
            <h2> CRM - Manager</h2>
        </div>
    </div>
</div>

<div class="container ">
    <table>
        <tr>
            <th> First Name</th>
            <th> Last Name</th>
            <th> Email </th>
        </tr>

        <c:forEach var="customer" items="${customers}">
            <tr>
                <td> ${customer.firstName} </td>
                <td> ${customer.lastName}  </td>
                <td> ${customer.email}     </td>
            </tr>
        </c:forEach>
    </table>
</div>


</body>
</html>
